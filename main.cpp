#include "stdc++.h"
#include "utils.h"
using namespace Eigen;
using namespace std;
int main(int argc, char **argv)
{
  // printf("%d\n", argc);
  if ((argc == 2) || (argc == 4 && argv[1] != "-r"))
  {
    return EXIT_FAILURE;
  }

  int opt;
  bool r = false;
  while ((opt = getopt(argc, argv, "r")) != -1)
  {
    if (opt != 'r')
      return EXIT_FAILURE;
  }

  int num = 0;
  vector<asset> A;
  MatrixXd Corr;
  char *universfile;
  char *corrfile;
  if (r == true)
  {
    universfile = argv[2];
    corrfile = argv[3];
  }
  else
  {
    universfile = argv[1];
    corrfile = argv[2];
  }
  A = read_universe(universfile, num);
  Corr = read_corr(corrfile, num);

  portfolio P;
  P.n = num;
  P.A = A;
  P.Corr = Corr;

  cout << "ROR,volatility" << endl;
  if (r == false)
  {
    for (double l = 0.01; l <= 0.265; l += 0.01)
    {
      cout << fixed << setprecision(1) << l * 100 << "%,";
      cout << fixed << setprecision(2) << unrestrict_opt(P, l) * 100 << "%" << endl;
    }
  }

  if (r == true)
  {
    for (double l = 0.01; l <= 0.265; l += 0.01)
    {
      cout << fixed << setprecision(1) << l * 100 << "%,";
      cout << fixed << setprecision(2) << restrict_opt(P, l) * 100 << "%" << endl;
    }
  }
  return 0;
}