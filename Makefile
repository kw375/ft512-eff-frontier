CCFLAGS=--std=c++11
efficient_frontier: utils.o main.o
	g++ -o efficient_frontier $(CCFLAGS) utils.o main.o
utils.o: utils.cpp
	g++ -c $(CCFLAGS) -o utils.o utils.cpp
main.o: main.cpp
	g++ -c $(CCFLAGS) -o main.o main.cpp

cleanall:
	rm -f *.o  *~ efficient_frontier

clean:
	rm -f *.o  *~
