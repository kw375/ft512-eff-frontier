#ifndef __UTILS_CPP__
#define __UTILS_CPP__
#include "stdc++.h"
#include "utils.h"
using namespace std;
using namespace Eigen;
class asset
{
public:
    string name;
    double mean;
    double sigma;

public:
    asset() {}
    ~asset() {}
};
class portfolio
{
public:
    int n;
    VectorXd weight;
    vector<asset> A;
    double sigma;
    double ror;
    MatrixXd Corr;

public:
    portfolio() {}
    void calculate_ror()
    {
        int sum = 0;
        for (int i = 0; i < n; i++)
        {
            sum += weight(i) * A[i].mean;
        }
        ror = sum;
    }
    void calculate_sigma()
    {
        double sum = 0;
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                sum += weight(i) * weight(j) * Corr(i, j) * A[i].sigma * A[j].sigma;
            }
        }
        sigma = sqrt(sum);
    }
    ~portfolio() {}
};

void split_string(string &s, vector<string> &v, string &c)
{
    string::size_type pos1, pos2;
    pos2 = s.find(c);
    pos1 = 0;
    while (string::npos != pos2)
    {
        v.push_back(s.substr(pos1, pos2 - pos1));

        pos1 = pos2 + c.size();
        pos2 = s.find(c, pos1);
    }
    if (pos1 != s.length())
        v.push_back(s.substr(pos1));
}

vector<asset> read_universe(char *filename, int &num)
{
    vector<asset> A;
    ifstream ifs(filename);
    if (!ifs.is_open())
    {
        exit(EXIT_FAILURE);
    }
    string line;
    string c = ",";
    while (getline(ifs, line))
    {
        vector<string> t;
        split_string(line, t, c);
        if (t.size() != 3)
        {
            exit(EXIT_FAILURE);
        }
        asset temp;
        temp.name = t[0];
        if (atof(t[1].c_str()) == 0.0 || atof(t[2].c_str()) == 0.0)
        {
            exit(EXIT_FAILURE);
        }
        temp.mean = atof(t[1].c_str());
        temp.sigma = atof(t[2].c_str());
        num++;
        A.push_back(temp);
    }
    if (num == 0)
    {
        exit(EXIT_FAILURE);
    }
    return A;
}

MatrixXd read_corr(char *filename, int num)
{
    ifstream ifs(filename);
    if (!ifs.is_open())
    {
        exit(EXIT_FAILURE);
    }

    MatrixXd Corr(num, num);
    string line;
    string c = ",";
    int i = 0;
    while (getline(ifs, line))
    {
        if (i == num)
        {
            exit(EXIT_FAILURE);
        }
        vector<string> t;
        split_string(line, t, c);
        if (t.size() != (size_t)num)
        {
            exit(EXIT_FAILURE);
        }
        for (int j = 0; j < num; j++)
        {
            if (atof(t[j].c_str()) == 0.0)
            {
                exit(EXIT_FAILURE);
            }
            Corr(i, j) = atof(t[j].c_str());
        }
        i++;
    }
    if (i == 0)
    {
        exit(EXIT_FAILURE);
    }
    for (int i = 0; i < num; i++)
    {
        for (int j = 0; j <= i; j++)
        {
            if (fabs(Corr(i, j) - Corr(j, i)) > 0.0001 || fabs(Corr(i, j)) > 1.0001)
            {
                exit(EXIT_FAILURE);
            }
        }
        if (fabs(Corr(i, i) - 1) > 0.0001)
        {
            exit(EXIT_FAILURE);
        }
    }
    return Corr;
}

double unrestrict_opt(portfolio &P, double &ret)
{
    int n = P.n;
    MatrixXd A1 = MatrixXd::Ones(1, n);
    MatrixXd A2(1, n);
    for (int i = 0; i < n; i++)
    {
        A2(i) = P.A[i].mean;
    }
    MatrixXd A(2, n);
    A << A1, A2;
    MatrixXd b1 = MatrixXd::Zero(n, 1);
    MatrixXd b2(2, 1);
    b2 << 1, ret;
    MatrixXd B(n + 2, 1);
    B << b1, b2;
    MatrixXd Cov(n, n);
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            Cov(i, j) = P.A[i].sigma * P.Corr(i, j) * P.A[j].sigma;
        }
    }
    MatrixXd O = MatrixXd::Zero(2, 2);
    MatrixXd K(n + 2, n + 2);
    K << Cov, A.transpose(), A, O;
    VectorXd X(n + 2, 1);
    X = K.fullPivHouseholderQr().solve(B);
    P.weight = X.head(n);
    P.calculate_sigma();
    return P.sigma;
}

double restrict_opt(portfolio &P, double &ret)
{
    int n = P.n;
    MatrixXd A1 = MatrixXd::Ones(1, n);
    MatrixXd A2(1, n);
    for (int i = 0; i < n; i++)
    {
        A2(i) = P.A[i].mean;
    }
    MatrixXd A(2, n);
    A << A1, A2;
    MatrixXd b1 = MatrixXd::Zero(n, 1);
    MatrixXd b2(2, 1);
    b2 << 1, ret;
    MatrixXd B(n + 2, 1);
    B << b1, b2;
    MatrixXd Cov(n, n);
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            Cov(i, j) = P.A[i].sigma * P.Corr(i, j) * P.A[j].sigma;
        }
    }
    MatrixXd O = MatrixXd::Zero(2, 2);
    MatrixXd K(n + 2, n + 2);
    K << Cov, A.transpose(), A, O;
    VectorXd X(n + 2, 1);
    X = K.fullPivHouseholderQr().solve(B);
    MatrixXd AA = A;
    MatrixXd BB = B;
    VectorXd XX = X;
    for (int i = 1;; i++)
    {
        int flag = 1;
        MatrixXd C;
        int k = 0;
        for (int j = 0; j < n; j++)
        {
            if (XX(j) < 0)
            {
                MatrixXd T = MatrixXd::Zero(n, 1);
                T(j, 0) = 1;
                MatrixXd temp = C;
                C.resize(n, k + 1);
                if (k == 0)
                {
                    C << T;
                }
                else
                {
                    C << temp, T;
                }
                flag = 0;
                k++;
            }
        }
        if (flag == 1)
        {
            break;
        }
        MatrixXd T;
        T = AA;
        AA.resize(T.rows() + C.cols(), n);
        AA << T, C.transpose();
        T = BB;
        BB.resize(T.rows() + C.cols(), 1);
        BB << T, MatrixXd::Zero(C.cols(), 1);
        MatrixXd OO = MatrixXd::Zero(AA.rows(), AA.rows());
        MatrixXd KK(Cov.rows() + AA.rows(), Cov.rows() + AA.rows());
        KK << Cov, AA.transpose(), AA, OO;
        XX = KK.fullPivHouseholderQr().solve(BB);
    }
    P.weight = XX.head(n);
    P.calculate_sigma();
    return P.sigma;
}

#endif
