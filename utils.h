#ifndef __UTILS_CPP__
#define __UTILS_CPP__
#include "stdc++.h"
using namespace std;
using namespace Eigen;
class asset
{
public:
    string name;
    double mean;
    double sigma;

public:
    asset() {}
    ~asset() {}
};
class portfolio
{
public:
    int n;
    VectorXd weight;
    vector<asset> A;
    double sigma;
    double ror;
    MatrixXd Corr;

public:
    portfolio() {}
    void calculate_ror();
    void calculate_sigma();
    ~portfolio() {}
};

void split_string(string &s, vector<string> &v, string &c);
vector<asset> read_universe(char *filename, int &num);
MatrixXd read_corr(char *filename, int num);

double unrestrict_opt(portfolio &P, double &ret);

double restrict_opt(portfolio &P, double &ret);

#endif
